
import Products from "../Products";

const HomePage = ({products,addToCart,removeFromCart,cart,favourite,addToFavourite,removeFromFavourite}) =>{
    
    return(
        <>
            <Products 
                products={products}
                addToCart={addToCart} 
                removeFromCart={removeFromCart}
                cart ={cart} 
                favourite={favourite}
                addToFavourite = {addToFavourite}
                removeFromFavourite = {removeFromFavourite}
            />
        
        </>
    )
}

export default HomePage;