import {Link} from 'react-router-dom'
import '../Favourite/Favourite.scss'



const Favourite = (props) =>{
   const {favCount} = props;
        return(
            <>
            <Link to='/favourite' className="favourite__wrap">
                <img className="favourite__wrap-img" src="/img/star-header.png" alt="star"/>
                <p className="favourite__count">({favCount})</p>
            </Link>
            </>
        )
}

export default Favourite;