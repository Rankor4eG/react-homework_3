import CartItem from './components/CartItem/CartItem'
import '../CartPage/CartPage.scss'

const CartPage = ({cartItemId,removeFromCart}) =>{


const productInCart = cartItemId.map(inCarts =>{
        return (
            <CartItem 
            title={inCarts.title} 
            url={inCarts.url} 
            color={inCarts.color} 
            oncklick={()=>{console.log('delete');}}
            key = {inCarts.id} 
            id={inCarts.id}
            price={inCarts.price}
            removeFromCart={removeFromCart}
            product={inCarts}
            />
        )
})

    return(
        <>
            <div className='cart-page'>
            <h2 className="cart-page__title">CARS IN CART</h2>
                <div className='cart-page__container' >
                    {productInCart}
                </div>
                
            </div>
        </>
    )
}

export default CartPage;