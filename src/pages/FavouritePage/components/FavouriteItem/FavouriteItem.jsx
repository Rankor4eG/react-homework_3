import { useState } from 'react'
import Modal from '../../../../components/Modal'
import Button from '../../../../components/Button'
import '../FavouriteItem/FavouriteItem.scss'



const  FavouriteItem = ({isCardInCart, product, title,url,color,id,price,removeFromFavourite,inFavourite,addToCart}) =>{
    
    const [modalDelFav,setModalDelFav] = useState(false)
    const [modalAddCart,setModalAddCart] = useState(false)


    const deleteFromFav = () =>{
        removeFromFavourite(inFavourite)
    }
    const addCart = () =>{
        if(isCardInCart){
            removeFromFavourite(product)
        }
        else {
            addToCart(inFavourite)
        }
    }
    const actionsCart = 
    <div className="modal__footer">
          <Button className='button__modal-cart--footer' onClick={addCart} text='ADD'/>
          <Button className='button__modal-cart--footer' onClick={()=>{setModalAddCart(false)}} text='CANCEL'/>
    </div>
    const actionsFav = 
    <div className="modal__footer">
            <Button className='button__modal-cart--footer' onClick={deleteFromFav} text='DELETE'/>
            <Button className='button__modal-cart--footer' onClick={()=>{setModalDelFav(false)}} text='CANCEL'/>
    </div>

    return (
    
    <>
        
        <div className="card">
            <div className="card__header">
                <img className="card__header-img" src={url} alt="img" />
                <div className="favourite__icon-wrapper">
                    
                    <img className="favourite__icon" 
                        src={'/img/star-active.png'} 
                        alt="star" 
                        onClick={()=>{setModalDelFav(true)}}
                        />
                        {modalDelFav && 
                            <Modal
                                header='Do you want delete this product from favourite?'
                                text='This product will be added to the favorite!'
                                btnClose={()=>{setModalDelFav(false)}}
                                actions={actionsFav}
                            />
                        }
                </div>
            </div>

            <div className="favourite-item__content">
                <p className="favourite-item__content-title">{title}</p>
                <p className="favourite-item__content-color">Color: {color}</p>
                <p className="favourite-item__content-article">Product id: {id}</p>
            </div>
            <div className="favourite-item__footer">
                <p className="favourite__footer-price">Price: {price}</p>
                <Button className='card__footer-button' onClick={()=>{setModalAddCart(true)}}  text = 'add to cart'/>
                    {modalAddCart &&
                        <Modal
                            header='Do you want to add this product to the cart?'
                            text='This product will be added to the cart!'
                            btnClose={()=>{setModalAddCart(false)}}
                            actions={actionsCart}
                        />
                    
                    }
            </div>
        </div>
    </>
    
    )
}
export default FavouriteItem;