import { useEffect,useState  } from 'react';
import { Route, Routes  } from 'react-router-dom';
import Header from './components/Header';
import HomePage from './components/HomePage/HomePage';
import CartPage from './pages/CartPage/CartPage';
import FavouritePage from './pages/FavouritePage/FavouritePage';
import './App.scss';


const App = () =>{

const [cart,setCart] = useState(localStorage.getItem('cart')  ? JSON.parse(localStorage.getItem('cart')) : [])
const [favourite,setFavourite] = useState(localStorage.getItem('fav') ? JSON.parse(localStorage.getItem('fav')) : [])
const [products,setProducts] = useState([])

useEffect(()=>{
  fetchData();
},[])

const fetchData = async () => {
  let response = await (
    await fetch('/products.json')
  ).json();
  setProducts(response);
};

const addToCart=(id)=>{
 
    setCart(
       [...cart,id]
    )
 
  localStorage.setItem('cart', JSON.stringify([...cart, id]))
}
const removeFromCart =(id)=>{
 setCart(
    
    cart.filter((cartItem)=> cartItem !== id)
  )
  localStorage.setItem('cart', JSON.stringify(cart.filter((cartItem)=> cartItem !== id)))
}
const addToFavourite = (id) => {
  setFavourite(
    [...favourite, id]
  )
  localStorage.setItem('fav', JSON.stringify([...favourite, id]))
}
 const removeFromFavourite = (id) => {
  setFavourite(
    
    favourite.filter((fav) => fav !== id)
  )
  localStorage.setItem('fav', JSON.stringify(favourite.filter((fav) => fav !== id)))


}

return(
  <>
     <div className="App">
          <Header cartCount={cart.length} favCount={favourite.length}/>
          <Routes>
            <Route path='/'  element={
              <HomePage
              products = {products}
                    addToCart={addToCart} 
                    removeFromCart={removeFromCart} 
                    cart ={cart} 
                    favourite={favourite}
                    addToFavourite = {addToFavourite}
                    removeFromFavourite = {removeFromFavourite}/>
                  } 
                />
                <Route path='/home' element={
                    <HomePage
                    products= {products}
                      addToCart={addToCart} 
                      removeFromCart={removeFromCart} 
                      cart ={cart} 
                      favourite={favourite}
                      addToFavourite = {addToFavourite}
                      removeFromFavourite = {removeFromFavourite}/> 
                      } 
                  />
                  <Route path='/cart' element={<CartPage removeFromCart={removeFromCart} cartItemId={cart}/>} />
                  <Route path='/favourite' element={
                        <FavouritePage
                        cartItem={cart}
                        products= {products}
                        addToCart={addToCart} 
                        removeFromCart={removeFromCart}  
                        favourite={favourite}
                        addToFavourite = {addToFavourite}
                        removeFromFavourite = {removeFromFavourite}
                        />
                  
                  } />
          </Routes>
          
    </div>
    
  </>
)
}

export default App;


